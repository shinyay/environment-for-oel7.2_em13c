#
# Cookbook Name:: common-packages
# Recipe:: default
#
# Copyright 2015, YOUR_COMPANY_NAME
#
# All rights reserved - Do Not Redistribute
#
pkgs = [
  "zip",
  "unzip"
]

pkgs.each do |pkg|
  package pkg do
    action :install
  end
end
